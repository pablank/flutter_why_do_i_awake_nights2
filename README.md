# Why do I stay awake night (app not released yet)
## Cross-platform application for insomnia control, 
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

![Иллюстрация к проекту](https://gitlab.com/pablank/flutter_why_do_i_awake_nights2/-/raw/master/readme_images/700573418.jpeg)

App was made by Bekoev team, by using Flutter.

## App Features
- Indicate sleepless nights, for analysis
- Indicate what you were doing instead of sleeping, what you were thinking about
- Specify tags for analysis
- Find out why you are awake most of the time
- Find people who can't sleep and start chatting with them inside app

## Application settings
- You can choose the color theme you like
- Application is localized, you can choose 
suitable language from the list 🇬🇧, 🇷🇺, 🇪🇸, 🇨🇳

## Development
At the moment the application is developed only by [ME](https://www.linkedin.com/in/pavel-bekoev-a60968261/)

## Future development
Sync with your smart watch so you can point to sleepless nights without having to go to the app

## Installation
Soon you will be able to install the game from PlayMarket and AppStore




